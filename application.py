import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None)
    username = os.environ.get("USER", None)
    password = os.environ.get("PASSWORD", None)
    hostname = os.environ.get("HOST", None)
    return db, username, password, hostname

@app.route('/drop_table', methods=['GET'])
def drop_table():
    db, username, password, hostname = get_db_creds()
    table_drop = 'DROP TABLE `movies`;'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    try:
        cur.execute(table_drop)
        cnx.commit()
        return get_and_render_all_movies(message="Successfully dropped table")
    except mysql.connector.Error as err:
        return get_and_render_all_movies(message=str(err.msg))


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            return get_and_render_all_movies(message="Error while creating table: table already exists.")
        else:
            return get_and_render_all_movies(message="Error while creating table: %s" % (str(exp.msg)))


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    #cur.execute("INSERT INTO movies (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movies")
        all_result = cur.fetchall()
        entries = [dict(year=row[1], title=row[2], director=row[3], actor=row[4], release_date=row[5], rating=row[6]) for row in all_result]
        return entries

    except Exception as exp:
        print(exp)
        return None

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

@app.route('/add_movie', methods=['POST'])
def add_movie():
    actor = request.form['actor']
    director = request.form['director']
    title = request.form['title']
    release_date = request.form['release_date']
    year = int(request.form['year'])
    rating = float(request.form['rating'])

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return render_template('index.html', message="Error occured while inserting movie with title %s: %s" % (title, str(exp.msg)))

    try:
        cur = cnx.cursor()
        insert_cmd = "INSERT INTO movies (year, title, director, actor, release_date, rating) values (%d, \"%s\", \"%s\", \"%s\", \"%s\", %f)" % (year, title, director, actor, release_date, rating)
        cur.execute(insert_cmd)
        cnx.commit()
        success_msg = "Movie %s successfully inserted" % (title)
        print (success_msg)
        return get_and_render_all_movies(message=success_msg)

    except Exception as exp:
        print(exp)
        return render_template('index.html', message="Error occured while inserting movie with title %s: %s" % (title, str(exp.msg)))

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    # print(request.form['message'])
    # msg = request.form['message']

    print ("request from client:")
    print(request.form)

    actor = request.form['actor']
    director = request.form['director']
    title = request.form['title']
    release_date = request.form['release_date']
    year = int(request.form['year'])
    rating = float(request.form['rating'])

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return render_template('index.html', message="Could not connect to database")

    try:
        cur = cnx.cursor()
        search_cmd = "SELECT * FROM movies WHERE LOWER(title) LIKE LOWER(\"%s\")" % (title)
        cur.execute(search_cmd)
        if (len(cur.fetchall()) == 0):
            return get_and_render_all_movies(message="Movies with title %s does not exist" % (title))    
        

        update_cmd = "UPDATE movies SET year = %d, director = \"%s\", actor = \"%s\", release_date = \"%s\", rating = %f WHERE LOWER(title) LIKE LOWER(\"%s\")" % (year, director, actor, release_date, rating, title)
        cur.execute(update_cmd)
        cnx.commit()
        success_msg = "Movie %s successfully updated" % (title)
        print (success_msg)
        return get_and_render_all_movies(message=success_msg)

    except Exception as exp:
        print(exp)
        return render_template('index.html', message="Error occured while updating movies with title %s" % (title, str(exp.msg)))

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    print ("request from client -- SEARCH:")
    actor = request.args['search_actor']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return render_template('index.html', message="Could not connect to database")

    try:
        cur = cnx.cursor()
        search_cmd = "SELECT * FROM movies WHERE LOWER(actor) LIKE LOWER(\"%s\")" % (actor)
        cur.execute(search_cmd)
        all_result = cur.fetchall()
        entries = [dict(year=row[1], title=row[2], director=row[3], actor=row[4], release_date=row[5], rating=row[6]) for row in all_result]
        if (len(entries) == 0):
            return render_template('index.html', entries=entries, message="No movies found for actor %s" % (actor))
        else:
            return render_template('index.html', entries=entries)

    except Exception as exp:
        print(exp)
        return render_template('index.html', message="Error occured while searching for movies of actor %s" % (actor, str(exp.msg)))

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    print ("request from client -- DELTE:")
    print(request.form)

    title = request.form['delete_title']
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return render_template('index.html', message="Could not connect to database")

    try:
        cur = cnx.cursor()
        search_cmd = "SELECT * FROM movies WHERE LOWER(title) LIKE LOWER(\"%s\")" % (title)
        cur.execute(search_cmd)
        if (len(cur.fetchall()) == 0):
            return get_and_render_all_movies(message="Movies with title %s does not exist" % (title))    

        delete_cmd = "DELETE FROM movies WHERE LOWER(title) LIKE LOWER(\"%s\")" % (title)
        cur.execute(delete_cmd)
        cnx.commit()
        success_msg = "Movie %s successfully deleted" % (title)
        print (success_msg)
        return get_and_render_all_movies(message=success_msg)

    except Exception as exp:
        print(exp)
        return render_template('index.html', message="Movie %s could not be deleted: %s" % (title, str(exp.msg)))

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return render_template('index.html', message="Could not connect to database")

    try:
        cur = cnx.cursor()
        highest_rating_cmd = "SELECT * FROM movies WHERE rating = (SELECT MAX(rating) FROM movies)"
        cur.execute(highest_rating_cmd)
        all_result = cur.fetchall()
        entries = [dict(year=row[1], title=row[2], director=row[3], actor=row[4], release_date=row[5], rating=row[6]) for row in all_result]
        return render_template('index.html', entries=entries, message="Movie(s) with highest rating:")

    except Exception as exp:
        print(exp)
        return render_template('index.html', message="Errors occured while retrieving highest rating movies: " + str(exp.msg))

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return render_template('index.html', message="Could not connect to database")

    try:
        cur = cnx.cursor()
        highest_rating_cmd = "SELECT * FROM movies WHERE rating = (SELECT MIN(rating) FROM movies)"
        cur.execute(highest_rating_cmd)
        all_result = cur.fetchall()
        entries = [dict(year=row[1], title=row[2], director=row[3], actor=row[4], release_date=row[5], rating=row[6]) for row in all_result]
        return render_template('index.html', entries=entries, message="Movie(s) with lowest rating:")

    except Exception as exp:
        print(exp)
        return render_template('index.html', message="Errors occured while retrieving lowest rating movies: " + str(exp.msg))

@app.route("/")
def get_and_render_all_movies(message = None):
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    if entries is None:
        entries = []
    print("Entries: %s" % entries)
    if message:
        return render_template('index.html', entries=entries, message=message)    
    else:
        return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')